PROJECT_NAME := imtrim
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>

CBINS := imtrim

imtrim_PKG_CONFIG := imlib2

all: $(CBINS)
