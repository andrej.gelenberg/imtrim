#include <Imlib2.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdarg.h>

struct trim_t {
  uint8_t t: 1;
  uint8_t b: 1;
  uint8_t r: 1;
  uint8_t l: 1;
};
typedef struct trim_t trim_t;

struct rect_t {
  size_t x;
  size_t y;
  size_t w;
  size_t h;
};
typedef struct rect_t rect_t;

struct rgba_t {
  union {
    struct {
      uint8_t r;
      uint8_t g;
      uint8_t b;
      uint8_t a;
    }  __attribute__((packed));
    uint32_t val;
  };
} __attribute__((packed));
typedef struct rgba_t rgba_t;

static char v = 0;

#define verbose(msg, ...) if ( v ) { fprintf(stderr, msg, ##__VA_ARGS__); }

static void get_roi(rect_t *rect, trim_t trim) {
  register size_t x1, y1, x2, y2, ix, iy;
  size_t width, height;
  rgba_t ref;
  rgba_t *data = (rgba_t*)imlib_image_get_data_for_reading_only();
  rgba_t pixel;

  width = imlib_image_get_width();
  height = imlib_image_get_height();

  verbose("original image: width: %zd height: %zd\n", width, height);

  ref = *data;

  x1 = width;
  y1 = height;
  x2 = y2 = 0;

  for(iy = 0; iy < height; ++iy) {
    for(ix = 0; ix < width; ++ix) {

      pixel.val = data[width*iy + ix].val;
      if ( (pixel.a > 13) && (pixel.val != ref.val) ) {
        if ( ix < x1 ) x1 = ix;
        if ( iy < y1 ) y1 = iy;
        if ( ix > x2 ) x2 = ix;
        if ( iy > y2 ) y2 = iy;
      }

    }
  }

  verbose("roi: (%zu,%zu) (%zu,%zu)\n", x1, y1, x2, y2);

  rect->x = trim.l ? x1 : 0;
  rect->y = trim.t ? y1 : 0;
  rect->w = trim.r ? x2 - rect->x : width - rect->x;
  rect->h = trim.b ? y2 - rect->y : height - rect->y;
}

static int print_crop(const char *in, trim_t trim) {
  __label__ end, err_end;
  int ret;
  Imlib_Image in_img;
  rect_t rect;

  verbose("print crop geometry of %s\n", in);

  in_img = imlib_load_image(in);
  if ( !in_img ) {
    fprintf(stderr, "can't load image '%s'\n", in);
    goto err_end;
  }
  imlib_context_set_image(in_img);

  get_roi(&rect, trim);

  printf("%zux%zu+%zu+%zu\n",
    rect.w, rect.h, rect.x, rect.y);

  ret = 0;
  goto end;
err_end:
  ret = 1;
end:
  return ret;
}

static int trim_files(const char *in, const char *out, trim_t trim) {
  __label__ end, err_end;
  int ret;
  Imlib_Image in_img, out_img = NULL;
  rect_t rect;

  verbose("convert %s -> %s\n", in, out);

  in_img = imlib_load_image(in);
  if ( !in_img ) {
    fprintf(stderr, "can't load image '%s'\n", in);
    goto err_end;
  }
  imlib_context_set_image(in_img);

  get_roi(&rect, trim);

  out_img = imlib_create_cropped_image(rect.x, rect.y, rect.w, rect.h);
  if ( !out_img ) {
    fprintf(stderr, "can't crop image\n");
    goto err_end;
  }

  imlib_context_set_image(out_img);
  imlib_save_image(out);

  ret = 0;
  goto end;
err_end:
  ret = 1;
end:
  if ( out_img )
    imlib_free_image();
  if ( in_img ) {
    imlib_context_set_image(in_img);
    imlib_free_image();
  }

  return ret;
}

static void usage(const char *prog) {
  printf("usage: %s [options] <input> [<output>]\n"
         "  if no output given, just print crop geometry\n"
         "  options:\n"
         "    -t: don't trim top\n"
         "    -b: don't trim bottom\n"
         "    -r: don't trim right\n"
         "    -l: don't trim left\n"
         "    -v: verbose output\n",
         prog);
}

int main(int argc, char **argv) {
  __label__ end, err_end;
  int ret;
  int opt;
  trim_t trim = { 1, 1, 1, 1 };

  if ( argc < 2 ) {
    usage(*argv);
    goto err_end;
  }

  while((opt = getopt(argc, argv, "tbrlv")) != -1) {
    switch(opt) {
      case 't':
        trim.t = 0;
        break;
      case 'b':
        trim.b = 0;
        break;
      case 'r':
        trim.r = 0;
        break;
      case 'l':
        trim.l = 0;
        break;
      case 'v':
        v = 1;
        break;
      default:
        usage(*argv);
        goto err_end;
    }
  }

  argc -= optind;

  switch ( argc ) {
    case 1:
      ret = print_crop(argv[optind], trim);
      break;
    case 2:
      ret = trim_files(argv[optind], argv[optind+1], trim);
      break;
    default:
      usage(*argv);
      goto err_end;
  }

  goto end;
err_end:
  ret = 1;
end:
  return ret;
}
